const { models } = require('../middleware/controllers')

let getModels = (path, req, res) => {
    let space = path || req.query.space
    let array = req.query.name ? req.query.name.split(',') : []
    models.get(space, array)
        .then(result => res.status(200).json(result))
        .catch(e =>  res.status(400).json({ message: `${e}`}))
}

module.exports.get = getModels