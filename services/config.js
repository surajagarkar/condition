'use strict'
const { config, logger } = require('../middleware/controllers')
const multer = require('multer')

let manageError = (res, error) => {
    console.log(error)
    logger.error('generic', error)
        .catch(e => console.log(e))
    res.status(400).json({ error })
}

let getConfig = (req, res) => {
    config.read()
        .then(result => res.json(result))
        .catch(e => manageError(res, e))
}

let getConfigPart = (req, res) => {
    let part = req.params.part
    config.readPart(part)
        .then(result => res.json(result))
        .catch(e => manageError(res, e))
}

let putConfig = (req, res) => {
    if (req.body && Object.keys(req.body).length) {
        config.update(req.body)
            .then(() => config.refresh())
            .then(() => res.json({ success: true }))
            .catch(e => manageError(res, e))
    }
    else manageError(res, 'Body empty or not found')
}

let putConfigPart = (req, res) => {
    let part = req.params.part
    if (req.body && Object.keys(req.body).length) {
        config.updatePart(part, req.body)
            .then(() => config.refresh())
            .then(() => res.json({ success: true, part }))
            .catch(e => manageError(res, e))
    }
    else manageError(res, 'Body empty or not found')
}

let importCert = (req, res) => {
    let part = req.params.part
    let storage = multer.diskStorage({
        destination: `./config/certificates/${part}/`,
        filename: (req, file, cb) => cb(null, file.fieldname + '.pem')
    })
    let upload = multer({ storage: storage })
    upload.fields([{ name: 'client', maxCount: 1 }, { name: 'client_key', maxCount: 1 }])(req, res, () => {
        config.refresh()
        res.json({ success: true, part })
    })
}

module.exports.get = getConfig
module.exports.getPart = getConfigPart
module.exports.put = putConfig
module.exports.putPart = putConfigPart
module.exports.import = importCert
