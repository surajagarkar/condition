/**
* @description Check condition module. 
* @author RoxAI - Atul Gaikwad & Nacho Bibián
* @date 04/07/2019
*/
const { logger} = require('../middleware/controllers')
const conditionController = require('../middleware/controllers').condition

let manageError = (res,step, error) => {
    //logger.error(step, error)
    res.status(400).json({ step, error })
}

//check function
let run = (req, res) => {
    let {data, condition, history} = req.body
    if(data && condition){
        conditionController.check(data, condition, history)
        .then(result => {
            res.json({result, data, condition, history})
        })
        .catch(e => manageError(res,'check', e))
    }
    else manageError(res,'',{error: 'data and condition are required fields'})
}
//let system = ...
//let data = ...


/*
data options: 
vs manual
vs last scanned +-%
vs 2nd measure
vs last N scanned +-%
on Nth percentile last M scanned
vs standard deviation last N scanned +-%
on Nth percentile drilled dimension
vs standard deviation drilled dimension

*/

module.exports.run = run;
