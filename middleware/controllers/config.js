'use strict'

const functions = require('../functions')
const cache = require('./cache')
const logger = require('./logger')

let updateConfig = (body) => {
    return new Promise((resolve, reject) => {
        try {
            let config = require('../../config/file.json')
            Object.keys(config).forEach(field => {
                if (body[field] && typeof config[field] === typeof body[field]) {
                    config[field] = body[field]
                }
            })
            let path = './config/file.json'
            functions.write(path, config)
                .then(() => resolve(true))
                .catch((e) => reject(e))
        }
        catch (e) { reject(e) }
    })
}

let updatePartConfig = (part, body) => {
    return new Promise((resolve, reject) => {
        try {
            let config = require('../../config/file.json')
            Object.keys(config[part]).forEach(field => {
                if (body[field] && typeof config[part][field] === typeof body[field]) {
                    config[part][field] = body[field]
                }
            })
            let path = './config/file.json'
            functions.write(path, config)
                .then(() => resolve(true))
                .catch((e) => reject(e))
        }
        catch (e) { reject(e) }
    })
}

let readConfig = () => {
    return new Promise((resolve, reject) => {
        try {
            let config = require('../../config/file.json')
            resolve(config)
        }
        catch (e) { reject(e) }
    })
}

let readPartConfig = (part) => {
    return new Promise((resolve, reject) => {
        try {
            let config = require('../../config/file.json')
            if (config.hasOwnProperty(part)) resolve(config[part])
            else reject(part + ' not found in config')
        }
        catch (e) { reject(e) }
    })
}

let refreshConfig = () => {
    let config = require('../../config/file.json')
    cache.post('config', config)
    logger.connect()
}
refreshConfig() //init
console.log('Init config')

module.exports.update = updateConfig
module.exports.updatePart = updatePartConfig
module.exports.read = readConfig
module.exports.readPart = readPartConfig
module.exports.refresh = refreshConfig
