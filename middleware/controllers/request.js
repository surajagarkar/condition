const request = require('request')
const functions = require('../functions')
const { name, version } = functions.readPackage

/**
 * @description sends GET request
 * @param {object} options request with values
 * @returns Promise with body {string} 
 */
let getRequest = (options) => {
    return new Promise((resolve, reject) => {
        if (options.headers) options.headers.microservice = name
        else {
            options.headers = { microservice: name }
        }
        request.get(options,
            (err, res, body) => {
                if (err) reject(err)
                resolve(body)
            }
        )
    })
}

/**
 * @description sends GET request
 * @param {object} options request with values
 * @returns Promise with body {string} 
 */
let deleteRequest = (options) => {
    return new Promise((resolve, reject) => {
        if (options.headers) options.headers.microservice = name
        else {
            options.headers = { microservice: name }
        }
        request.delete(options,
            (err, res, body) => {
                if (err) reject(err)
                resolve(body)
            }
        )
    })
}

/**
 * @description sends POST request
 * @param {object} default request with default values
 * @param {string} requestUrl
 * @param {object} body
 * @returns Promise with body {string} 
 */
let postRequest = (options, payload) => {
    return new Promise((resolve, reject) => {
        if (options.headers) options.headers.microservice = name
        else {
            options.headers = { microservice: name }
        }
        if (payload) {
            options.body = JSON.stringify(payload)
            options.headers['content-type'] = 'application/json'
        }
        request.post(options,
            (err, res, body) => {
                if (err) reject(err)
                resolve(body)
            }
        )
    })
}

/**
 * @description sends POST request
 * @param {object} default request with default values
 * @param {string} requestUrl
 * @param {object} body
 * @returns Promise with body {string} 
 */
let putRequest = (options, payload) => {
    return new Promise((resolve, reject) => {
        if (options.headers) options.headers.microservice = name
        else {
            options.headers = { microservice: name }
        }
        if (payload) {
            options.body = JSON.stringify(payload)
            options.headers['content-type'] = 'application/json'
        }
        request.put(options,
            (err, res, body) => {
                if (err) reject(err)
                resolve(body)
            }
        )
    })
}

module.exports.get = getRequest
module.exports.delete = deleteRequest
module.exports.post = postRequest
module.exports.put = putRequest
