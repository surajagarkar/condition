const _ = require('lodash')

const cache = require('./cache')
const request = require('./request')
const functions = require('../functions')
const { name, version } = functions.readPackage

let loggerConfig

let connect = () => {
    const { https, ip, port } = cache.get('config').repository
    loggerConfig = {
        rejectUnauthorized: false,
        uri: `${https ? 'https' : 'http'}://${ip}:${port}/logs`,
        host: ip,
        port: port
    }
}

let payload = {
    microservice: name,
    version: version
}

let logError = (type, error) => {
    let options = _.cloneDeep(loggerConfig)
    let body = _.cloneDeep(payload)
    body.type = type
    body.error = error
    return request.post(options, body)
}

let logPost = (message) => {
    let options = _.cloneDeep(loggerConfig)
    let body = _.cloneDeep(payload)
    body.message = message
    return request.post(options, body)
}

module.exports.connect = connect
module.exports.error = logError
module.exports.post = logPost
