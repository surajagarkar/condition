'use strict'

let applyLogic = (string) => {
    return new Promise((resolve, reject) => {
        let array = string.match(/\w+/g).filter(x => !x.match(/(true|false|and|or)/i))
        if (array.length) reject(`Forbidden elements in logic string: ${array.join(', ')}`)
        let condition = string.replace(/and/gi, '&&').replace(/or/gi, '||')
        try { resolve(eval(condition)) }
        catch (e) { reject(`Could not evaluate string to boolean: ${string}`) }
    })
}

/**
 * @param {*} value
 * @param {String} operator
 * @param {*} compare
 * @returns {Boolean}
 */
let evaluate = (value, operator, compare) => {
    try {
        let regex
        switch (operator) {
            case 'eq':
                return value == compare
            case 'ne':
                return value != compare
            case 'gt':
                return value > compare
            case 'gte':
            case 'ge':
                return value >= compare
            case 'lt':
                return value < compare
            case 'lte':
            case 'le':
                return value <= compare
            case 'in':
                if (Array.isArray(compare)) {
                    return compare.includes(value)
                }
                else return false
            case 'sw':
                regex = new RegExp('^' + compare)
                return Boolean(value.match(regex))
            case 'si'://case insensitive
                regex = new RegExp('^' + compare, 'i')
                return Boolean(value.match(regex))
            case 'ew':
                regex = new RegExp(compare + '$')
                return Boolean(value.match(regex))
            case 'ei'://case insensitive
                regex = new RegExp(compare + '$', 'i')
                return Boolean(value.match(regex))
            case 'so'://substring
            case 'ss':
                return Boolean(value.match(compare))
            case 'soi'://substring case insensitive
            case 'ssi':
                regex = new RegExp(compare, 'i')
                return Boolean(value.match(regex))
            default:
                return false
        }
    }
    catch (e) {
        console.log(e)
        return false
    }
}

/**
 * @param {object} data
 * @param {object} condition
 */
let check = (data, condition, history) => {
    //return Promise.resolve(Math.random() > 0.5)
    return new Promise((resolve, reject) => {
        try {
            let plus
            let minus
            switch (condition.type) {
                case 'manual':
                    resolve(evaluate(data, condition.operator, condition.value))
                    break
                case 'measure':
                    resolve(evaluate(data[0], condition.operator, data[1]))
                    break
                case 'last':
                    plus = data[1] * (1 + condition.range / 100)
                    minus = data[1] * (1 - condition.range / 100)
                    let result = evaluate(data, 'ge', plus) || evaluate(data, 'le', minus)
                    resolve(result)
                    break
                case 'lastPlus':
                    plus = data[1] * (1 + condition.range / 100)
                    resolve(evaluate(data, 'ge', plus))
                    break
                case 'lastMinus':
                    minus = data[1] * (1 - condition.range / 100)
                    resolve(evaluate(data, 'le', minus))
                    break
                default:
                    reject('invalid condition.type')
            }
        }
        catch (e) { reject(e) }
    })
}

module.exports.applyLogic = applyLogic
module.exports.check = check
