const memoryCache = require('memory-cache')
let expiration = 30 * 60 * 1000

let getCache = (key) => memoryCache.get(key)

let putCache = (key, data) => {
    memoryCache.put(key, data, expiration, () => { console.log(`expired cache for ${key}`) })
}

let postCache = (key, data) => memoryCache.put(key, data)

let deleteCache = (key) => memoryCache.del(key)

module.exports.get = getCache
module.exports.put = putCache
module.exports.post = postCache
module.exports.delete = deleteCache
