/**
 * 
 * @description Generic useful functions
 */
const fs = require('fs')
const path = require('path')

/**
 * @description generate random xrfkey
 */
let xrfkey = Math.random().toString(36).substring(2, 10) + Math.random().toString(36).substring(2, 10)

let certificatesPath = "../config/certificates"
/**
 * @param {string} filename 
 */
const readCert = (filename, folder) => fs.readFileSync(path.resolve(__dirname, certificatesPath, folder, filename))

/**
 * @param {string} filename 
 */
const readFile = (route, filename) => fs.readFileSync(path.resolve(route, filename))

/**
 * 
 */
const readConfig = require('../config/file.json')

/**
 * 
 */
const readPackage = require('../package.json')

/**
 * 
 */
const checkCerts = () => {
    return new Promise((resolve, reject) => {
        let key = fs.existsSync(path.resolve(__dirname, certificatesPath, 'client_key.pem'))
        let cert = fs.existsSync(path.resolve(__dirname, certificatesPath, 'client.pem'))
        if (key && cert) resolve(true)
        else reject('Certificates not found')
    })
}

module.exports.xrfkey = xrfkey
module.exports.readCert = readCert
module.exports.readFile = readFile
module.exports.readConfig = readConfig
module.exports.readPackage = readPackage
module.exports.checkCerts = checkCerts