/**
* @description Routes handler. This file handles all http requests.
* @author RoxAI - Nacho Bibián
* @date 03/07/2019
*/
'use strict'

// NPM modules
const bodyParser = require('body-parser') // Json parse for express
// Custom Modules
const { models, condition } = require('../services') // controller index

/** 
* Function than handles all request routes
* @param app {}, a representation of an express app needed to handle routes
*/
let handler = app => {
    // Set Body-Parser for APP
    let jsonParser = bodyParser.json()
    // Access Control
    app.all('*', (req, res, next) => {
        console.log('hit '+req.originalUrl)
        // TODO: Access control, if required
        next()
    })
    //POST Request condition.
    app.post(/^\/(check|condition)+$/, jsonParser, (req, res) => {
        condition.run(req, res)
    })
    app.post('/import/:part', (req, res) => {
        config.import(req, res)
    })
    app.get('/config', (req, res) => {
        config.get(req, res)
    })
    app.get('/config/:part', (req, res) => {
        config.getPart(req, res)
    })
    app.put('/config', jsonParser, (req, res) => {
        config.put(req, res)
    })
    app.put('/config/:part', jsonParser, (req, res) => {
        config.putPart(req, res)
    })
    // Default 404 error for all requests not catched before
    app.all('*', (req, res) => res.status(404).json('Not found'))
}

exports.handler = handler