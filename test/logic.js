let {applyLogic} = require('../middleware/controllers/condition')

let string = '(true or false) and (true or false)'

applyLogic(string)
    .then(r => console.log(r))
    .catch(error => console.log({ error }))