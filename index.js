/*
* Index file for Qlik Connector. 
* @author RoxAI - Nacho Bibián
* @date 03/07/2019
*/
'use strict'

//Built-in Modules
const http = require('http')
const https = require('https')
// NPM Modules
const express = require('express')
const cors = require('cors') // Cors for express
// Internal Modules
const routes = require('./middleware/routes')
const functions = require('./middleware/functions')
const { httpPort, httpsPort } = require('./config/file')

// Express Init
const app = express()
app.set('env', 'production') // Set env to production to avoid showing trace errors
app.use(cors())

// Endpoint Handler 
routes.handler(app)
// WebServer HTTP
http.createServer(app).listen(httpPort, () => {
  console.log(`Condition Module started at ${new Date().toLocaleString()}`)
  console.log(`PID: ${process.pid}.`)
  console.log(`HTTP Port: ${httpPort}`)
})
try{
  let httpsOptions = {
    key: functions.readCert('client_key.pem','self'),
    cert: functions.readCert('client.pem','self')
  }
  https.createServer(httpsOptions,app).listen(httpsPort, ()=> {
    console.log(`HTTPS Port: ${httpsPort}`)  })
}
catch(e){console.log('Couldn\'t start HTTPS: \n'+e+'\n')}

// process.on('unhandledRejection', (reason, p) => {
//   console.log('Unhandled Rejection at: Promise', p, 'reason:', reason)
// })
